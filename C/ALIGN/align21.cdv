/*    ALIGN21
TESTING align CLAUSE
arrA2[BLOCK][BLOCK] arrB1[]*/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>

static void align211();
static void align212();
static void align213();
static void align214();

static void ansyes(const char tname[]);
static void ansno(const char tname[]);

static int NL = 10000;
static int ER= 100000;
static int s, cs, erri, i, j, ia, ja, ib, jb;

int main(int an, char **as)
{
    printf("=== START OF ALIGN21 ======================\n");
/* ALIGN arrB[i] WITH arrA[1][i]   vector arrB on section
                                              (the first line of arrA)*/
    align211();
/* ALIGN arrB[i] WITH arrA[2 * i + 2][2] vector arrB on section
             (the second column of arrA) with stretching and shift*/
    align212();
/* ALIGN arrB[i] WITH arrA[][i] vector replication on every line of arrA*/
    align213();
/* ALIGN arrB[i] WITH arrA[2 * i + 2][] vector arrB on replication on
             every column of arrA with stretching and shift*/
    align214();

    printf("=== END OF ALIGN21 ========================\n");
    return 0;
}
/* ---------------------------------------------ALIGN211  */
/* ALIGN arrB[i] WITH arrA[1][i]   vector arrB on section
                                 (the first line of arrA)*/
void align211()
{
/*    parameters for ALIGN arrB[i] WITH arrA[1][i]*/
    #define AN1 8
    #define AN2 8
    #define BN1 4
    int k1i = 0, k2i = 0, li = 1;
    int k1j = 1, k2j = 0, lj = 0;

    #pragma dvm array distribute[block][block]
    int A2[AN1][AN2];
    #pragma dvm array align([i] with A2[1][i])
    int B1[BN1];
    char tname[] = "align211";

    erri = ER;
    #pragma dvm actual(erri)
    #pragma dvm region local(A2, B1)
    {
    #pragma dvm parallel([i] on B1[i])
    for (i = 0; i < BN1; i++)
        B1[i] = 0;
    #pragma dvm parallel([i][j] on A2[i][j]) private(ib)
    for (i = 0; i < AN1; i++)
        for (j = 0; j < AN2; j++)
        {
            A2[i][j] = i * NL + j;
            if ((i == 1) && (j < BN1))
            {
                ib = j;
                B1[ib] = ib;
            }
        }
    #pragma dvm parallel([i] on B1[i]) reduction(min(erri)), private(ia, ja)
    for (i = 0; i < BN1; i++)
    {
        if (B1[i] != i)
            if (erri > i) erri = i;
        ia = 1;
        ja = i;
        if (A2[ia][ja] != (ia * NL + ja))
            if (erri > i * NL / 10 + j) erri = i * NL / 10 + j;
    }
    }
    #pragma dvm get_actual(erri)

    if (erri == ER)
        ansyes(tname);
    else
        ansno(tname);
    #undef AN1
    #undef AN2
    #undef BN1
}
/* ---------------------------------------------ALIGN212*/
/* ALIGN arrB[i] WITH arrA[2*i+2][2] vector arrB on section
                    (the second column of arrA) with stretching and shift*/
void align212()
{
    /*     parameters for ALIGN arrB[i] WITH arrA[2*i+2][2]*/
    int AN1 = 14;
    int AN2 = 3;
    int BN1 = 6;
    int k1i = 2, k2i = 0, li = 2;
    int k1j = 0, k2j = 0, lj = 2;
    char tname[] = "align212";

    #pragma dvm array distribute[block][block]
    int (*A2)[AN2];
    #pragma dvm array align([i] with A2[k1i * i + li][lj])
    int *B1;

    A2 = malloc(sizeof(int[AN1][AN2]));
    B1 = malloc(sizeof(int[BN1]));

    erri = ER;
    #pragma dvm actual(erri)
    #pragma dvm region local(A2, B1)
    {
    #pragma dvm parallel([i] on B1[i])
    for (i = 0; i < BN1; i++)
        B1[i] = 0;
    #pragma dvm parallel([i][j] on A2[i][j]) private(ib)
    for (i = 0; i < AN1; i++)
        for (j = 0; j < AN2; j++)
        {
            A2[i][j] = i * NL + j;
            if (j == lj){
                if (((i - li) == (((i - li) / k1i) * k1i)) &&
                    (((i - li) / k1i) >= 0) &&
                    (((i - li) / k1i) < BN1))
                {
                    ib = (i - li) / k1i;
                    B1[ib] = ib;
                }
            }
        }
    #pragma dvm parallel([i] on B1[i]) reduction(min(erri)), private(ia, ja)
    for (i = 0; i < BN1; i++)
    {
        if (B1[i] != i)
            if (erri > i) erri = i;
        ia = k1i * i + li;
        ja = lj;
        if (A2[ia][ja] != (ia * NL + ja))
            if (erri > i * NL / 10 + j) erri = i * NL / 10 + j;
    }
    }
    #pragma dvm get_actual(erri)

    if (erri == ER)
        ansyes(tname);
    else
        ansno(tname);

    free(B1);
    free(A2);
}
/* ---------------------------------------------ALIGN213*/
/* ALIGN arrB[i] WITH arrA[][i]  vector replication on every line of arrA*/
void align213()
{
/*     parameters for ALIGN arrB[i] WITH arrA[][k1j * i + lj]*/
    #define AN1 8
    #define AN2 8
    #define BN1 6
    int k1i = 0, k2i = 0, li = 0;
    int k1j = 1, k2j = 0, lj = 0;

    #pragma dvm array distribute[block][block]
    int A2[AN1][AN2];
    #pragma dvm array align([i] with A2[][k1j * i + lj])
    int B1[BN1];
    char tname[] = "align213";

    erri = ER;
    s = 0;
    #pragma dvm actual(erri, s)
    #pragma dvm region local(A2, B1)
    {
    #pragma dvm parallel([i] on B1[i])
    for (i = 0; i < BN1; i++)
        B1[i] = i;
    #pragma dvm parallel([i][j] on A2[i][j]) reduction(min(erri)), private(ib)
    for (i = 0; i < AN1; i++)
        for (j = 0; j < AN2; j++)
        {
            A2[i][j] = i * NL + j;
            if (((j - lj) == (((j - lj) / k1j) * k1j)) &&
                (((j - lj) / k1j) >= 0) &&
                (((j - lj) / k1j) < BN1))
            {
                ib = (j - lj) / k1j;
                if (B1[ib] != ib)
                    if (erri > ib) erri = ib;
            }
        }
    #pragma dvm parallel([i] on B1[i]) reduction(min(erri), sum(s))
    for (i = 0; i < BN1; i++)
    {
        s = s + B1[i];
        if (B1[i] != i)
            if (erri > i) erri = i;
    }
    }
    #pragma dvm get_actual(erri, s)

    cs = (0 + BN1-1) * BN1 / 2;
    if ((erri == ER) && (s == cs))
        ansyes(tname);
    else
    {
        ansno(tname);
//        printf("%d, %d, %d\n", erri, s, cs);
    }
    #undef AN1
    #undef AN2
    #undef BN1
}
/* ---------------------------------------------ALIGN214*/
/* ALIGN arrB[i] WITH arrA[2*i+2][ ] vector arrB on replication on
             every column of arrA with stretching and shift*/
void align214()
{
/*     parameters for ALIGN arrB[i] WITH arrA[k1i*i+li][]*/
    int AN1 = 28;
    int AN2 = 8;
    int BN1 = 5;
    int k1i = 2, k2i = 0, li = 2;
    int k1j = 0, k2j = 0, lj = 0;
    char tname[] = "align214";

    #pragma dvm array distribute[block][block]
    int (*A2)[AN2];
    #pragma dvm array align([i] with A2[k1i * i + li][])
    int *B1;

    A2 = malloc(sizeof(int[AN1][AN2]));
    B1 = malloc(sizeof(int[BN1]));

    erri = ER;
    s = 0;
    #pragma dvm actual(erri, s)
    #pragma dvm region local(A2, B1)
    {
    #pragma dvm parallel([i] on B1[i])
    for (i = 0; i < BN1; i++)
        B1[i] = i;
    #pragma dvm parallel([i][j] on A2[i][j]) reduction(min(erri)), private(ib)
    for (i = 0; i < AN1; i++)
        for (j = 0; j < AN2; j++)
        {
            A2[i][j] = i * NL + j;
            if (((i - li) == (((i - li) / k1i) * k1i)) &&
                (((i - li) / k1i) >= 0) &&
                (((i - li) / k1i) < BN1))
            {
                ib = (i - li) / k1i;
                if (B1[ib] != ib)
                    if (erri > i) erri = i;
            }
        }
    #pragma dvm parallel([i] on B1[i]) reduction(sum(s))
    for (i = 0; i < BN1; i++)
        s = s + B1[i];
    }
    #pragma dvm get_actual(erri, s)

    cs = (0 + BN1-1) * BN1 / 2;
    if ((erri == ER) && (s == cs))
        ansyes(tname);
    else
        ansno(tname);

    free(B1);
    free(A2);
}

void ansyes(const char name[])
{
    printf("%s  -  complete\n", name);
}

void ansno(const char name[])
{
    printf("%s  -  ***error\n", name);
}
